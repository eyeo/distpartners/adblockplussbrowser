/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.adblockplus.adblockplussbrowser.base.samsung.constants

object SamsungInternetConstants {
    const val SBROWSER_APP_NAME = "sbrowser"
    const val SBROWSER_APP_ID = "com.sec.android.app.sbrowser"
    const val SBROWSER_APP_ID_BETA = "com.sec.android.app.sbrowser.beta"
    const val SBROWSER_ACTION_OPEN_SETTINGS = "com.samsung.android.sbrowser.contentBlocker.ACTION_SETTING"
    const val SBROWSER_OLDEST_SAMSUNG_INTERNET_4_VERSIONCODE = 400000000
    const val SBROWSER_START_SETTINGS_DELAY = 500L
}
