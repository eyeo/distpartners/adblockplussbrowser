<?xml version="1.0" encoding="utf-8"?>
<!--
 This file is part of Adblock Plus <https://adblockplus.org/>,
 Copyright (C) 2006-present eyeo GmbH

 Adblock Plus is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 as
 published by the Free Software Foundation.

 Adblock Plus is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
  -->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <include
            android:id="@+id/onboarding_aa_header_include"
            layout="@layout/onboarding_header"
            android:layout_width="0dp"
            android:layout_height="112dp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <ImageView
            android:id="@+id/onboarding_aa_annoying_ads_image"
            android:contentDescription="@string/onboarding_acceptable_ads_annoying_ads_title"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/onboarding_content_top_margin"
            android:src="@drawable/ic_onboarding_block_ads"
            app:layout_constraintStart_toStartOf="@id/onboarding_aa_guideline_start"
            app:layout_constraintTop_toBottomOf="@id/onboarding_aa_header_include" />

        <TextView
            android:id="@+id/onboarding_aa_annoying_ads_title"
            style="@style/OnboardingTitleTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="@dimen/onboarding_items_margin_start"
            android:text="@string/onboarding_acceptable_ads_annoying_ads_title"
            app:layout_constraintEnd_toEndOf="@id/onboarding_aa_guideline_end"
            app:layout_constraintStart_toEndOf="@id/onboarding_aa_annoying_ads_image"
            app:layout_constraintTop_toTopOf="@id/onboarding_aa_annoying_ads_image" />

        <TextView
            android:id="@+id/onboarding_aa_annoying_ads_description"
            style="@style/OnboardingDescriptionTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/onboarding_item_description_margin_top"
            android:text="@string/onboarding_acceptable_ads_annoying_ads_description"
            app:layout_constraintEnd_toEndOf="@id/onboarding_aa_guideline_end"
            app:layout_constraintStart_toStartOf="@id/onboarding_aa_annoying_ads_title"
            app:layout_constraintTop_toBottomOf="@id/onboarding_aa_annoying_ads_title" />

        <androidx.constraintlayout.widget.Barrier
            android:id="@+id/onboarding_aa_annoying_ads_barrier"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:barrierDirection="bottom"
            app:constraint_referenced_ids="onboarding_aa_annoying_ads_image, onboarding_aa_annoying_ads_description" />

        <ImageView
            android:id="@+id/onboarding_aa_nonintrusive_ads_image"
            android:contentDescription="@string/onboarding_acceptable_ads_nonintrusive_ads_title"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="24dp"
            android:src="@drawable/ic_onboarding_acceptable_ads"
            app:layout_constraintStart_toStartOf="@id/onboarding_aa_guideline_start"
            app:layout_constraintTop_toBottomOf="@id/onboarding_aa_annoying_ads_barrier" />

        <TextView
            android:id="@+id/onboarding_aa_nonintrusive_ads_title"
            style="@style/OnboardingTitleTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="@dimen/onboarding_items_margin_start"
            android:text="@string/onboarding_acceptable_ads_nonintrusive_ads_title"
            app:layout_constraintEnd_toEndOf="@id/onboarding_aa_guideline_end"
            app:layout_constraintStart_toEndOf="@id/onboarding_aa_nonintrusive_ads_image"
            app:layout_constraintTop_toTopOf="@id/onboarding_aa_nonintrusive_ads_image" />

        <TextView
            android:id="@+id/onboarding_aa_nonintrusive_ads_description"
            style="@style/OnboardingDescriptionTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/onboarding_item_description_margin_top"
            android:text="@string/onboarding_acceptable_ads_nonintrusive_ads_description"
            app:layout_constraintEnd_toEndOf="@id/onboarding_aa_guideline_end"
            app:layout_constraintStart_toStartOf="@id/onboarding_aa_nonintrusive_ads_title"
            app:layout_constraintTop_toBottomOf="@id/onboarding_aa_nonintrusive_ads_title" />

        <TextView
            android:id="@+id/onboarding_aa_nonintrusive_ads_example"
            style="@style/OnboardingDescriptionTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:paddingTop="@dimen/onboarding_item_description_margin_top"
            android:paddingBottom="@dimen/onboarding_item_description_margin_top"
            android:text="@string/onboarding_acceptable_ads_nonintrusive_ads_example"
            android:textColor="@color/foreground_accent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="@id/onboarding_aa_nonintrusive_ads_description"
            app:layout_constraintTop_toBottomOf="@id/onboarding_aa_nonintrusive_ads_description" />

        <androidx.constraintlayout.widget.Barrier
            android:id="@+id/onboarding_aa_nonintrusive_ads_barrier"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:barrierDirection="bottom"
            app:constraint_referenced_ids="onboarding_aa_nonintrusive_ads_image, onboarding_aa_nonintrusive_ads_example" />

        <ImageView
            android:id="@+id/onboarding_aa_disable_image"
            android:contentDescription="@string/onboarding_acceptable_ads_disable_title"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/onboarding_margin"
            android:src="@drawable/ic_onboarding_disable_anytime"
            app:layout_constraintStart_toStartOf="@id/onboarding_aa_guideline_start"
            app:layout_constraintTop_toBottomOf="@id/onboarding_aa_nonintrusive_ads_barrier" />

        <TextView
            android:id="@+id/onboarding_aa_disable_title"
            style="@style/OnboardingTitleTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="@dimen/onboarding_items_margin_start"
            android:text="@string/onboarding_acceptable_ads_disable_title"
            app:layout_constraintEnd_toEndOf="@id/onboarding_aa_guideline_end"
            app:layout_constraintStart_toEndOf="@id/onboarding_aa_disable_image"
            app:layout_constraintTop_toTopOf="@id/onboarding_aa_disable_image" />

        <TextView
            android:id="@+id/onboarding_aa_disable_description"
            style="@style/OnboardingDescriptionTextStyle"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/onboarding_item_description_margin_top"
            android:text="@string/onboarding_acceptable_ads_disable_description"
            app:layout_constraintEnd_toEndOf="@id/onboarding_aa_guideline_end"
            app:layout_constraintStart_toStartOf="@id/onboarding_aa_disable_title"
            app:layout_constraintTop_toBottomOf="@id/onboarding_aa_disable_title" />

        <androidx.constraintlayout.widget.Guideline
            android:id="@+id/onboarding_aa_guideline_start"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:orientation="vertical"
            app:layout_constraintGuide_begin="@dimen/onboarding_guides_margin" />

        <androidx.constraintlayout.widget.Guideline
            android:id="@+id/onboarding_aa_guideline_end"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:orientation="vertical"
            app:layout_constraintGuide_end="@dimen/onboarding_guides_margin" />

    </androidx.constraintlayout.widget.ConstraintLayout>

</layout>